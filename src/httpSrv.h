#ifndef HTTP_SERVER_H
#define HTTP_SERVER_H

#include <ESP8266WebServer.h>

class HttpSrv
{
public:
    static HttpSrv &getInstance(bool modeIsConfig = false)
    {
        static HttpSrv instance(modeIsConfig);
        return instance;
    }
    int handleRequest();

private:
    bool runningInConfigMode;
    ESP8266WebServer server;

    HttpSrv(HttpSrv const &) = delete;
    void operator=(HttpSrv const &) = delete;

private:
    HttpSrv(bool);
};

#endif // HTTP_SERVER_H
