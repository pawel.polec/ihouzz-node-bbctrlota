#include "config.h"

#include <ArduinoJson.h>
#include <string>
#include <LittleFS.h>

constexpr int JSONDocuSize = 500;

Config::Config()
{
    bool somethingWentWrong = false;
    File file = LittleFS.open("/config.json", "r");
    StaticJsonDocument<JSONDocuSize> doc;
    if (file)
    {
        Serial.println("Config file opened");
        size_t fSize = file.size();
        char *buffor = new char[fSize + 1];
        file.readBytes(buffor, fSize);
        file.close();
        buffor[fSize] = '\0';
        auto r = deserializeJson(doc, buffor);
        if (r.code() != ARDUINOJSON_NAMESPACE::DeserializationError::Code::Ok)
        {
            Serial.print("!!!ERROR!!! deserializeJson failed: ");
            Serial.println(r.c_str());
            somethingWentWrong = true;
        }
        else
        {
            ssid = String(doc["wifi_ssid"].as<const char *>());
            pass = String((const char *)doc["wifi_pass"]);
        }
        delete[] buffor;
    }
    else
    {
        somethingWentWrong = true;
    }

    if (somethingWentWrong)
    {
        Serial.println("No config file present. Run in default settings");
        ssid = "";
        pass = "";
    }

    Serial.println("Config loaded:");
    Serial.print("  WiFi SSID: ");
    Serial.println(ssid);
    // Serial.print("  Wifi pass: ");Serial.println(pass);
}

void Config::save()
{
    File file = LittleFS.open("/config.json", "w");
    StaticJsonDocument<JSONDocuSize> doc;
    doc["wifi_ssid"] = ssid;
    doc["wifi_pass"] = pass;
    Serial.println("Saving: " + ssid + " " + pass);
    serializeJson(doc, file);
    file.close();
}
