#include "updater.h"

#include <ESP8266httpUpdate.h>

namespace Updater
{
    Results OTA::checkUpdate(bool rebootOnUpdate)
    {
        WiFiClientSecure client;
        client.setInsecure();
        if (!client.connect(host, httpsPort))
        {
            return Results::CONNECTION_FAILED;
        }

        ESPhttpUpdate.rebootOnUpdate(rebootOnUpdate);

        auto ret = ESPhttpUpdate.update(client, host, httpsPort, url, currentVersion);
        switch (ret)
        {
        case HTTPUpdateResult::HTTP_UPDATE_OK:
            return Results::OK;
        case HTTPUpdateResult::HTTP_UPDATE_FAILED:
            return Results::UPDATE_FAILED;

        case HTTPUpdateResult::HTTP_UPDATE_NO_UPDATES:
            return Results::UP_TO_DATE;

        default:
            return Results::UNKNOWN;
        }
    }

    OTA &OTA::setOptions(const String &_host, uint16_t _port, const String &_uri, const String &_currentVersion)
    {
        host = _host;
        httpsPort = _port;
        url = _uri;
        currentVersion = _currentVersion;

        return *this;
    }

} // namespace Updater
