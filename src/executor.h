#ifndef EXECUTOR_H
#define EXECUTOR_H

#include <vector>
#include <memory>
#include <functional>
#include <limits>

#include <Arduino.h>

namespace Executor
{
    constexpr uint8_t DEFAULT_RELAY_PIN = (0);

    enum class State : std::int8_t
    {
        OFF,
        ON,
        TOGGLE
    };

    class Relay
    {
    public:
        uint8_t pin = DEFAULT_RELAY_PIN;

        static Relay &getInstance(uint8_t pinNo = DEFAULT_RELAY_PIN)
        {
            static Relay instance(pinNo);
            return instance;
        }

        static void init(uint8_t pinNo = DEFAULT_RELAY_PIN)
        {
            getInstance(pinNo);
        }

        State getState() const { return currentState; }

        void shouldBeInverted(bool b)
        {
            inverted = b;
            setRelay();
        }

        void setState(State state);

        Relay(Relay const &) = delete;
        void operator=(Relay const &) = delete;

    private:
        Relay(uint8_t pinNo);

        void setRelay()
        {
            bool isOn = (currentState == State::ON);
            if (inverted)
                isOn = !isOn;
            digitalWrite(pin, (isOn) ? HIGH : LOW);
        }

        bool inverted = true;

        State currentState = State::OFF;
    };

}

#endif // EXECUTOR_H