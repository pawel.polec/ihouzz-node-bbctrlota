#define DEBUG_ESP_SSL 1

#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>
#include <LittleFS.h>
#include "ESP8266httpUpdate.h"
#include <ArduinoJson.h>
#include <string>
#include "config.h"
#include "httpSrv.h"

#include <sntp.h>
#include <TimeLib.h>
#include <TimeAlarms.h>
#include <coredecls.h>

#include "sensor.h"
#include "executor.h"
#include "updater.h"

const char *host = "ihouzz.ddns.net";
const int httpsPort = 443;

const char *url = "/fw.php?type=bbctrl_v1";

const char *APssid = "iHouzzNodeBangBangController";

#define currentVersion "0.2.0"

void setupAP()
{
  WiFi.mode(WIFI_AP);
  WiFi.softAPConfig(IPAddress(192, 168, 0, 1), IPAddress(192, 168, 0, 1), IPAddress(255, 255, 255, 0)); // subnet FF FF FF 00
  WiFi.softAP(APssid);

  IPAddress myIP = WiFi.softAPIP(); // Get IP address
  Serial.println("HotSpot IP:" + myIP.toString());
}

WiFiClientSecure client;

void setup()
{
  Serial.begin(115200);

  Sensor::Temperature::init();
  Executor::Relay::init();

  if (!LittleFS.begin())
  {
    Serial.println("An Error has occurred while mounting LittleFS");
    return;
  }

  delay(1000);

  bool setAP = true;
  if (Config::getInstance().getSSID() != "")
  {
    Serial.print("connecting to " + Config::getInstance().getSSID());
    WiFi.mode(WIFI_STA);
    if (WiFi.SSID() != Config::getInstance().getSSID())
    {
      WiFi.hostname("RelayNode");
      WiFi.begin(Config::getInstance().getSSID(), Config::getInstance().getPASS());
    }

    for (int attempts = 0; attempts < 30; ++attempts)
    {
      delay(500);
      Serial.print(".");
      if (WiFi.status() == WL_CONNECTED)
      {
        setAP = false;
        break;
      }
    }
  }

  if (setAP)
  {
    setupAP();
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // configure time
  configTime(3600, 0, "pool.ntp.org");
  settimeofday_cb([]
                  { setTime(sntp_get_current_timestamp() + 3600); });

  Updater::OTA::getInstance().setOptions(String(host), httpsPort, String(url), currentVersion); //.checkUpdate();
}

void printTime(time_t *tim)
{
  tmElements_t tms = {0}; // *timeinfo = localtime(tim);
  breakTime(now(), tms);
  char s[] = "00:00:00";

  snprintf(s, sizeof(s), "%02d:%02d:%02d", tms.Hour % 24, tms.Minute % 60, tms.Second % 60);

  Serial.println(s);
}

void loop()
{
  static time_t old_ti = 0;
  time_t ti;
  time(&ti);

  if (old_ti != ti)
  {
    printTime(&ti);
    old_ti = ti;
  }
  yield();

  Alarm.delay(0);

  HttpSrv::getInstance().handleRequest();

  // struct tm *timeinfo = localtime(&ti) ;
  // Serial.println(localtime(&ti)->tm_sec );
}