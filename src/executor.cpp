#include "executor.h"
#include <algorithm>

namespace Executor
{
    Relay::Relay(uint8_t pinNo)
    {
        pin = pinNo;
        pinMode(pin, OUTPUT);
        setState(State::OFF);
    };

    void Relay::setState(State state)
    {
        State newState = state;
        if (state == State::TOGGLE)
        {
            if (currentState == State::ON)
            {
                newState = State::OFF;
            }
            else
            {
                newState = State::ON;
            }
        }
        currentState = newState;
        setRelay();
    }

}
