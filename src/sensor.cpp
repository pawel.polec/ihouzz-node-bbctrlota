#include "sensor.h"
#include <algorithm>

#include <TimeLib.h>
#include <TimeAlarms.h>

namespace Sensor
{
    void onTimer()
    {
        Serial.println("Refreshing temp...");
        Temperature::getInstance().refreshTemp();
    }

    Temperature::Temperature(uint8_t pinNo)
    {
        oneWire.begin(pinNo);
        sensors.setOneWire(&oneWire);

        sensors.begin();
        sensors.setWaitForConversion(false);
        sensors.setResolution(12);

        refreshTemp();
        state = State::PROCESSING;

        Alarm.timerRepeat(60, onTimer);
    };

    void Temperature::refreshTemp()
    {
        sensors.requestTemperatures();
        conversionStarted = true;
    }

    void Temperature::precheck()
    {
        if (conversionStarted)
        {
            if (sensors.getDS18Count() == 0)
            {
                state = State::ERROR; // TODO: rethink how to recover
                return;
            }
            if (sensors.isConversionComplete())
            {
                currentTemp = sensors.getTempCByIndex(0);
                state = State::READY;
                conversionStarted = false;
            }
        }
    }
}
