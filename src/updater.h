#ifndef UPDATER_H
#define UPDATER_H

#include <TimeLib.h>
#include <TimeAlarms.h>

namespace Updater
{
    enum class Results
    {
        OK,
        CONNECTION_FAILED,
        UPDATE_FAILED,
        UP_TO_DATE,
        UNKNOWN
    };

    class OTA
    {
    public:
        static OTA &getInstance()
        {
            static OTA instance;
            return instance;
        }

        OTA &setOptions(const String &_host, uint16_t _port, const String &_uri, const String &_currentVersion);

        Results checkUpdate(bool rebootOnUpdate = true);

        OTA(OTA const &) = delete;
        void operator=(OTA const &) = delete;

    private:
        OTA() {}
        bool runningInConfigMode;

        String host = "ihouzz.ddns.net";
        int httpsPort = 443;
        String url = "/fw.php?type=common_v1";
        String currentVersion = "0.0.0";
    };

    //   // Use WiFiClientSecure class to create TLS connection
    //   //BearSSL::X509List x509(x509CA);

    //   //client.setTrustAnchors(&x509);
    //   client.setInsecure();
    //   yield();
    //   Serial.print("connecting to ");
    //   Serial.println(host);
    //   if (!client.connect(host, httpsPort)) {
    //     Serial.println("connection failed");
    //     return;
    //   }
    //   yield();

    //   Serial.print("Starting OTA from: ");
    //   Serial.println(url);

    //   yield();
    //   //auto ret = ESPhttpUpdate.update(client, host, url);
    //   auto ret = ESPhttpUpdate.update(client, host, httpsPort, url, currentVersion);
    //   // if successful, ESP will restart
    //   yield();
    //   Serial.println("update failed");
    //   Serial.println((int) ret);
    //   yield();
} // namespace Updater

#endif // UPDATER_H
