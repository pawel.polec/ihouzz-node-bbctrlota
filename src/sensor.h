#ifndef SENSOR_H
#define SENSOR_H

#include <vector>
#include <memory>
#include <functional>
#include <limits>

#include <OneWire.h>
#include <DallasTemperature.h>

namespace Sensor
{
    constexpr uint8_t DEFAULT_OW_PIN = (2);

    enum class State : std::int8_t
    {
        INIT,
        PROCESSING,
        READY,
        ERROR
    };

    class Temperature
    {
    public:
        friend void onTimer();

        static Temperature &getInstance(uint8_t pinNo = DEFAULT_OW_PIN)
        {
            static Temperature instance(pinNo);
            return instance;
        }

        static void init(uint8_t pinNo = DEFAULT_OW_PIN)
        {
            getInstance(pinNo);
        }

        float getCurrentTemp()
        {
            precheck();
            if (state != State::READY)
                return std::numeric_limits<double>::quiet_NaN();
            return currentTemp;
        }
        State getStatus() { return state; }

        Temperature(Temperature const &) = delete;
        void operator=(Temperature const &) = delete;

    private:
        Temperature(uint8_t pinNo);

        float currentTemp = std::numeric_limits<double>::quiet_NaN();
        bool conversionStarted = false;

        OneWire oneWire;
        DallasTemperature sensors;
        State state = State::INIT;

        void precheck();

        void refreshTemp();
    };

}

#endif // SENSOR_H